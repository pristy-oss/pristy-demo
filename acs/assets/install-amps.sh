#!/usr/bin/bash
set -e

export MMT_JAR=$(find "${TOMCAT_HOME}/alfresco-mmt/"  -type f -name "*.jar")
find "${TOMCAT_HOME}/webapps/alfresco" -type d -print0 | xargs -0 chmod 755
java -jar "${MMT_JAR}" install "${TOMCAT_HOME}/amps/" "${TOMCAT_HOME}/webapps/alfresco" \
  -nobackup -directory -force

find "${TOMCAT_HOME}/amps/" -type f -name "*.amp" -delete
