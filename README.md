# Pristy Demo

Quickly test all the applications in the Pristy suite.

## Quick Start

Prerequisites:

- docker 27+ (with compose)
- git
- at least 15GB of free space in /var/lib/docker
- at least 8GB of ram

```shell
git clone https://gitlab.com/pristy-oss/pristy-demo.git
cd pristy-demo
./start.sh
./stop.sh
./clean.sh # Clean will remove all data
```

## Add custom modules

If you need to install custom modules, copy your jar or amp files in `./acs/modules` folder. Then use helper script to
build and load your stack.

```shell
./pristy.sh build
./pristy.sh up -d
```

The default environment file used is `./docker/pristy.env`, you can use `./docker/vanilla.env` to load Alfresco
Community images. This is usefull to load amp modules into vanilla docker images.

```shell
export PRISTY_ENV=vanilla
./pristy.sh build
./pristy.sh up -d
```

For pristy developer, you can use `dev.sh` script with the environment name as parameter.

## Manage

`pristy.sh` is a wrapper on docker compose, so you can use standard docker-compose command.

```
./pristy.sh ps
./pristy.sh logs acs
./pristy.sh logs proxy
```

## Versions

| Version | Commits                                                                                                     |
|---------|:------------------------------------------------------------------------------------------------------------|
| 0.1.0   | First public release                                                                                        |
|         | Pristy Core 1.0.0, Pristy Espaces 0.3.0, Pristy Actes 0.3.0                                                 |
| 0.2.0   | Setup of Pristy-Portail                                                                                     |
|         | Pristy Core 1.0.0, Pristy Espaces 0.4.0, Pristy Actes 0.4.0, Pristy Portail 0.1.0                           |
| 0.3.0   | Setup of Pristy-Portail                                                                                     |
|         | Pristy Espaces 0.5.0, Pristy Portail 0.2.0                                                                  |
| 0.4.0   | Setup of Pristy Kafka 0.1.0                                                                                 |
|         | Pristy Core 1.1.0, Alfresco Collabora Online 0.7.0                                                          |
|         | Pristy Espaces 0.7.0, Pristy Actes 0.5.0, Pristy Portail 0.4.0                                              |
| 0.5.0   | Setup of Collabora Online sur Pristy                                                                        |
|         | Pristy Espaces 0.8.0, Pristy Actes 0.7.0, Pristy Portail 0.4.1                                              |
| 0.6.0   | Improvements for Pristy Espaces and Pristy Actes                                                            |
|         | Pristy Espaces 0.9.0, Pristy Actes 0.8.0, Pristy Portail 0.4.2                                              |
| 0.6.1   | Improvement of search and navigation                                                                        |
| 24.0    | Complete update of the stack for an external deployment                                                     |
|         | Pristy Espaces 1.7.1, Pristy Portail 0.12.0, ACS 7.4.2, Share 7.4.2.1, ASS 2.0.12, Transform Core Aio 5.1.5 |
|         | Additional elements : Collabora 24.04.9.2.1, Caddy 2.8.4, Postgres 14.12, Kafka 3.7.0                       |
|         | Addition of Mailhog and removal of ACA                                                                      |
|         | Creation of some accounts at startup                                                                        |
