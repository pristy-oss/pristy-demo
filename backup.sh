#!/usr/bin/env bash

./pristy.sh stop acs postgres

DIR="backup_$(date +%F-%H%M)"
mkdir "$DIR"


ACS_VOLUME=$(docker volume inspect pristy-demo_acs-volume -f '{{ index .Mountpoint }}')
echo "Backup $ACS_VOLUME"
sudo tar czf "$DIR/cs.tar.gz" "$ACS_VOLUME"

PG_VOLUME=$(docker volume inspect pristy-demo_postgres-volume -f '{{ index .Mountpoint }}')
echo "Backup $PG_VOLUME"
sudo tar czf "$DIR/pg.tar.gz" "$PG_VOLUME"

./pristy.sh start postgres
sleep 1
./pristy.sh start acs
