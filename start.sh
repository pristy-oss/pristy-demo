#!/usr/bin/env bash

PRISTY_ENV_FILE=${PRISTY_ENV:-pristy}.env

./pristy.sh pull
./pristy.sh build
./pristy.sh up -d

echo Continue
echo App running at: http://localhost:8080/
echo - Pristy - Portail:              http://localhost:8080/portail/
echo - Pristy - Espaces:              http://localhost:8080/espaces/
echo - Pristy - $PRISTY_ENV:          http://localhost:8080/$PRISTY_ENV/
echo - Alfresco Content Store:        http://localhost:8080/alfresco/
echo - Alfresco API Explorer:         http://localhost:8080/api-explorer/
echo - Solr 6:                        http://localhost:8080/solr/
echo

sleep 15

./pristy.sh run sample

echo
echo Available Users are :
echo - admin
echo - abe
echo - bart
echo - homer
echo - lisa
echo - maggie
echo - mona
echo
echo The password is same as login
