---
# Pristy Docker Compose Sample Project
services:
  proxy:
    image: docker.io/caddy:2.8.4-alpine
    mem_limit: 50m
    ports:
      - "8080:80"
    volumes:
      - "caddy-data:/data"
      - "caddy-config:/config"
      - "./Caddyfile:/etc/caddy/Caddyfile:ro"
      - "./public:/srv:ro"
    depends_on:
      - acs
      - pristy-espaces
      - pristy-portail
      - collabora
    networks:
      - backend

  postgres:
    image: docker.io/postgres:14.12
    mem_limit: 250m
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "alfresco"]
      interval: 10s
      retries: 5
      start_period: 5s
    environment:
      POSTGRES_DB: alfresco
      POSTGRES_USER: alfresco
      POSTGRES_PASSWORD: alfresco
    command: postgres -c max_connections=100 -c log_min_messages=LOG
    volumes:
      - postgres-volume:/var/lib/postgresql/data
    networks:
      - backend

  acs:
    build:
      dockerfile: ./Dockerfile
      context: ./acs
      args:
        IMAGE_FROM: ${ACS_IMAGE}
        IMAGE_TAG: ${ACS_TAG}
    mem_limit: 1.5g
    healthcheck:
      test: ["CMD", "curl", "--fail", "http://localhost:8080/alfresco/api/-default-/public/alfresco/versions/1/probes/-ready-"]
      interval: 10s
      retries: 5
      start_period: 15s
    environment:
      JAVA_OPTS: "-Djavamelody.authorized-users=monitor:monitor"
      CATALINA_OPTS: "-agentlib:jdwp=transport=dt_socket,address=*:8000,server=y,suspend=n"
    volumes:
      - acs-volume:/usr/local/tomcat/alf_data
    depends_on:
      - postgres
      - kafka
      - mailhog
      - collabora
    networks:
      - backend

  share:
    build:
      dockerfile: ./Dockerfile
      context: ./share
      args:
        IMAGE_FROM: ${SHR_IMAGE}
        IMAGE_TAG: ${SHR_TAG}
    mem_limit: 1g
    depends_on:
      - acs
    environment:
      REPO_HOST: "acs"
      REPO_PORT: 8080
      CSRF_FILTER_REFERER: "http://localhost:8080/.*"
      CSRF_FILTER_ORIGIN: "http://localhost:8080"
    networks:
      - backend

  solr:
    image: docker.io/alfresco/alfresco-search-services:2.0.12
    mem_limit: 2g
    environment:
      SOLR_ALFRESCO_HOST: acs
      SOLR_ALFRESCO_PORT: 8080
      SOLR_SOLR_HOST: solr
      SOLR_SOLR_PORT: 8983
      SOLR_CREATE_ALFRESCO_DEFAULTS: alfresco,archive
      ALFRESCO_SECURE_COMMS: "secret"
      JAVA_TOOL_OPTIONS: "-Dalfresco.secureComms.secret=secret"
    depends_on:
      - acs
    volumes:
      - solr-contentstore-volume:/opt/alfresco-search-services/contentstore
      - solr-data-volume:/opt/alfresco-search-services/data
      - solr-keystores-volume:/opt/alfresco-search-services/keystores
      - solr-solrhome-volume:/opt/alfresco-search-services/solrhome
    ports:
      - "8983:8983"
    networks:
      - backend

  transform-core-aio:
    image: docker.io/alfresco/alfresco-transform-core-aio:5.1.5
    mem_limit: 1g
    depends_on:
      - acs
    environment:
      JAVA_OPTS: " -XX:MinRAMPercentage=50 -XX:MaxRAMPercentage=80"
    networks:
      - backend

  collabora:
    image: collabora/code:24.04.9.2.1
    mem_limit: 1g
    privileged: true
    environment:
      extra_params: '--o:ssl.enable=false --o:server_name=localhost:9980'
    ports:
      - "9980:9980"
    networks:
      - backend

  pristy-portail:
    image: rg.fr-par.scw.cloud/pristy/pristy-portail:0.12.0
    mem_limit: 50m
    volumes:
      - ./public/env-config-portail.json:/srv/env-config.json:ro
    depends_on:
      - acs
    networks:
      - backend

  pristy-espaces:
    image: rg.fr-par.scw.cloud/pristy/pristy-espaces:1.7.1
    mem_limit: 50m
    volumes:
      - ./public/env-config-espaces.json:/srv/env-config.json:ro
    depends_on:
      - acs
    networks:
      - backend

  kafka:
    image: apache/kafka:3.7.0
    mem_limit: 800m
    ports:
      - "9092:9092"
    environment:
      KAFKA_NODE_ID: 1
      KAFKA_LISTENER_SECURITY_PROTOCOL_MAP: 'CONTROLLER:PLAINTEXT,PLAINTEXT:PLAINTEXT,PLAINTEXT_HOST:PLAINTEXT'
      KAFKA_ADVERTISED_LISTENERS: 'PLAINTEXT_HOST://localhost:19092,PLAINTEXT://kafka:9092'
      KAFKA_PROCESS_ROLES: 'broker,controller'
      KAFKA_CONTROLLER_QUORUM_VOTERS: '1@kafka:29093'
      KAFKA_LISTENERS: 'CONTROLLER://:29093,PLAINTEXT_HOST://:19092,PLAINTEXT://:9092'
      KAFKA_INTER_BROKER_LISTENER_NAME: 'PLAINTEXT'
      KAFKA_CONTROLLER_LISTENER_NAMES: 'CONTROLLER'
      KAFKA_OFFSETS_TOPIC_REPLICATION_FACTOR: 1
      KAFKA_GROUP_INITIAL_REBALANCE_DELAY_MS: 0
      KAFKA_TRANSACTION_STATE_LOG_MIN_ISR: 1
      KAFKA_TRANSACTION_STATE_LOG_REPLICATION_FACTOR: 1
    networks:
      - backend


  mailhog:
    image: mailhog/mailhog:v1.0.1
    ports:
      - "8025:8025"
    volumes:
      - mailhog-mail-volume:/maildir
    networks:
      - backend
    environment:
      MH_STORAGE: maildir

  sample:
    build:
      dockerfile: ./Dockerfile
      context: ./sample
    mem_limit: 50m
    depends_on:
      - acs
    networks:
      - backend


networks:
  backend:

volumes:
  caddy-data:
  caddy-config:
  acs-volume:
  postgres-volume:
  solr-contentstore-volume:
  solr-data-volume:
  solr-keystores-volume:
  solr-solrhome-volume:
  mailhog-mail-volume:
