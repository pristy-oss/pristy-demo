#!/usr/bin/env bash

export PRISTY_ENV=$1

NEXUS_API=https://nexus.jeci.tech/service/rest/v1
NEXUS_PUBLIC=https://nexus.jeci.tech/repository/maven-public
NEXUS_PRISTY=https://nexus.jeci.tech/repository/pristy-releases
NEXUS_SONARTYPE=https://nexus.jeci.tech/repository/sonatype-public
ACS_MODULES_DIR=acs/modules
env | grep VERSION

source docker/${PRISTY_ENV:-pristy}.env

CURL="curl -sSL --fail -u $NEXUS_USERNAME:$NEXUS_PASSWORD"

function curlSnapshotAmp() {
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=$1
  ARTIFACT_ID=$2
  APP_VERSION=$3
  echo "-- Download ${ARTIFACT_ID} ${APP_VERSION}"
  echo "   $NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${APP_VERSION}&maven.extension=amp"
  $CURL -H "accept: application/json" \
        -o "${ACS_MODULES_DIR}/${ARTIFACT_ID}-${APP_VERSION}.amp" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${APP_VERSION}&maven.extension=amp"
}

function curlAmp() {
  NEXUS_REPO=$1
  GROUP_ID=$2
  ARTIFACT_ID=$3
  APP_VERSION=$4
  EXT=$5
  echo "-- Download ${ARTIFACT_ID} ${APP_VERSION}"
  echo "   ${NEXUS_REPO}/$( echo $GROUP_ID | tr '.' '/')/${ARTIFACT_ID}/${APP_VERSION}/${ARTIFACT_ID}-${APP_VERSION}${EXT}.amp"
  $CURL -o "${ACS_MODULES_DIR}/${ARTIFACT_ID}-${APP_VERSION}.jar" \
        "${NEXUS_REPO}/$( echo $GROUP_ID | tr '.' '/')/${ARTIFACT_ID}/${APP_VERSION}/${ARTIFACT_ID}-${APP_VERSION}${EXT}.amp"
}

function curlSnapshotJar() {
  NEXUS_REPO=pristy-snapshots
  GROUP_ID=$1
  ARTIFACT_ID=$2
  APP_VERSION=$3
  echo "-- Download ${ARTIFACT_ID} ${APP_VERSION}"
  echo "   $NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${APP_VERSION}&maven.extension=jar"
  $CURL -H "accept: application/json" \
        -o "${ACS_MODULES_DIR}/${ARTIFACT_ID}-${APP_VERSION}.jar" \
        "$NEXUS_API/search/assets/download?sort=version&repository=${NEXUS_REPO}&maven.groupId=${GROUP_ID}&maven.artifactId=${ARTIFACT_ID}&maven.baseVersion=${APP_VERSION}&maven.extension=jar"
}

function curlJar() {
  NEXUS_REPO=$1
  GROUP_ID=$2
  ARTIFACT_ID=$3
  APP_VERSION=$4
  EXT=$5
  echo "-- Download ${ARTIFACT_ID} ${APP_VERSION}"
  echo "   ${NEXUS_REPO}/$( echo $GROUP_ID | tr '.' '/')/${ARTIFACT_ID}/${APP_VERSION}/${ARTIFACT_ID}-${APP_VERSION}${EXT}.jar"
  $CURL -o "${ACS_MODULES_DIR}/${ARTIFACT_ID}-${APP_VERSION}.jar" \
        "${NEXUS_REPO}/$( echo $GROUP_ID | tr '.' '/')/${ARTIFACT_ID}/${APP_VERSION}/${ARTIFACT_ID}-${APP_VERSION}${EXT}.jar"
}

if [[ "${JS_CONSOLE_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp de.fmaul javascript-console-repo "${JS_CONSOLE_VERSION}"
elif [[ -n "${JS_CONSOLE_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" de.fmaul javascript-console-repo "${JS_CONSOLE_VERSION}"
fi

if [[ -n "${OOTBEE_TOOLS_VERSION}" ]]
then
  curlAmp "${NEXUS_SONARTYPE}" org.orderofthebee.support-tools support-tools-repo ${OOTBEE_TOOLS_VERSION} "-amp"
fi

if [[ "${AUTH_HMAC_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco authentication-hmac-platform "${AUTH_HMAC_VERSION}"
elif [[ -n "${AUTH_HMAC_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco authentication-hmac-platform "${AUTH_HMAC_VERSION}"
fi

if [[ "${PRISTY_CORE_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco pristy-core-platform "${PRISTY_CORE_VERSION}"
elif [[ -n "${PRISTY_CORE_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco pristy-core-platform "${PRISTY_CORE_VERSION}"
fi

if [[ "${PRISTY_ACTES_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco pristy-actes "${PRISTY_ACTES_VERSION}"
elif [[ -n "${PRISTY_ACTES_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco pristy-actes "${PRISTY_ACTES_VERSION}"
fi

if [[ "${PRISTY_MARCHES_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco pristy-marches "${PRISTY_MARCHES_VERSION}"
elif [[ -n "${PRISTY_MARCHES_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco pristy-marches "${PRISTY_MARCHES_VERSION}"
fi

if [[ "${PRISTY_SOCIAL_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco pristy-social "${PRISTY_SOCIAL_VERSION}"
elif [[ -n "${PRISTY_SOCIAL_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco pristy-social "${PRISTY_SOCIAL_VERSION}"
fi

if [[ "${PRISTY_KAFKA_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco pristy-kafka-platform "${PRISTY_KAFKA_VERSION}"
elif [[ -n "${PRISTY_KAFKA_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco pristy-kafka-platform "${PRISTY_KAFKA_VERSION}"
fi

if [[ "${AUTH_PAC4J_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco alfresco-authentication-pac4j-platform "${AUTH_PAC4J_VERSION}"
elif [[ -n "${AUTH_PAC4J_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco alfresco-authentication-pac4j-platform "${AUTH_PAC4J_VERSION}"
fi

if [[ "${SEND_MAIL_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotAmp fr.jeci.alfresco send-mail-platform "${SEND_MAIL_VERSION}"
elif [[ -n "${SEND_MAIL_VERSION}" ]]
then
  curlAmp "${NEXUS_PRISTY}" fr.jeci.alfresco send-mail-platform "${SEND_MAIL_VERSION}"
fi

if [[ "${COLLAB_VERSION}" == *-SNAPSHOT ]]
then
  curlSnapshotJar fr.jeci.alfresco collabora-platform-extension "${COLLAB_VERSION}"
elif [[ -n "${COLLAB_VERSION}" ]]
then
  curlJar "${NEXUS_PRISTY}" fr.jeci.alfresco collabora-platform-extension "${COLLAB_VERSION}"
fi

if [[ -n "${CAMEL_VERSION}" ]]
then
  curlJar  "${NEXUS_PUBLIC}" org.apache.camel camel-vm "${CAMEL_VERSION}"
fi
if [[ -n "${ACTIVEMQ_VERSION}" ]]
then
  curlJar  "${NEXUS_PUBLIC}" org.apache.activemq activemq-broker "${ACTIVEMQ_VERSION}"
fi

#./start.sh
