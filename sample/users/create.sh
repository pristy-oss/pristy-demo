#!/usr/bin/env bash

for file in *.json
do
curl -sS -u admin:admin -X POST --header "Content-Type: application/json"  --data "@${file}" \
  http://acs:8080/alfresco/api/-default-/public/alfresco/versions/1/people
done
