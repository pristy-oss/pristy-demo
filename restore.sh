#!/usr/bin/env bash

DIR=$(ls -1tdr backup_* | tail -1)

echo "Restore $DIR"

./down.sh
./pristy.sh create -d acs postgres

ACS_VOLUME=$(docker volume inspect pristy-demo_acs-volume -f '{{ index .Mountpoint }}')
echo "Restore $ACS_VOLUME"
sudo tar xzvf "$DIR/cs.tar.gz" -C /

PG_VOLUME=$(docker volume inspect pristy-demo_postgres-volume -f '{{ index .Mountpoint }}')
echo "Restore $PG_VOLUME"
sudo tar xzvf "$DIR/pg.tar.gz" -C /


# ./pristy.sh create -d proxy
