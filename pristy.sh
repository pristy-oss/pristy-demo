#!/usr/bin/env bash

set -euo pipefail

PROJECT_DIR="$(cd "$(dirname "${BASH_SOURCE[0]}")" && pwd)"
PRISTY_ENV_FILE=${PRISTY_ENV:-pristy}.env

if [ $# -gt 0 ]; then
    exec docker compose --env-file docker/${PRISTY_ENV_FILE} "${@}"
else
    ./start.sh
fi